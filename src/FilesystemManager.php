<?php

namespace AliSaleem\Filesystem;

use Illuminate\Support\Arr;
use League\Flysystem\Filesystem;
use Spatie\Dropbox\Client;
use Spatie\FlysystemDropbox\DropboxAdapter;

/**
 * @mixin \Illuminate\Contracts\Filesystem\Filesystem
 */
class FilesystemManager extends \Illuminate\Filesystem\FilesystemManager
{
    public function createDropboxDriver(array $config)
    {
        $client = new Client(Arr::get($config, 'token'));
        $adapter = new DropboxAdapter($client);

        return $this->adapt(new Filesystem($adapter, ['case_sensitive' => false]));
    }

    public function dropbox()
    {
        $name = $this->getDefaultDropboxDriver();

        return $this->disks[$name] = $this->get($name);
    }

    public function getDefaultDropboxDriver()
    {
        return $this->app['config']['filesystems.dropbox'];
    }
}