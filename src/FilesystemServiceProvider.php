<?php

namespace AliSaleem\Filesystem;

use Illuminate\Support\ServiceProvider;

class FilesystemServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->extend('filesystem', function () {
            return new FilesystemManager($this->app);
        });
    }
}